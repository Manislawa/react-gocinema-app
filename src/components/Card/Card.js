import React from "react";
import RunningTime from '../RunningTime/RunningTime'
import Alert from '../Alert/Alert'
import Button from '../Button/Button'
import PropTypes from "prop-types";
import styles from "./Card.module.scss";


const Card = ({title, img, releaseDate, desc, ...restProps}) => {
  return (
    <div className={styles.container}>
      <Alert {...restProps} />
      <div className={styles.posterContainer}>
        <img className={styles.filmPoster} src={img} alt="plakat filmowy" />
      </div>
      <div className={styles.infoContainer}>
        <h3>{title}</h3>
        <p className={styles.releaseDate}>Premiera: {releaseDate} r.</p>
        <RunningTime {...restProps}/>
        <p>{desc}</p>
      </div>
      <div className={styles.sideContainer}>
        <div>ranking</div>
        <Button {...restProps}/>
      </div>
    </div>
  );
};

Card.defaultProps = {
  title: 'brak tytułu',
  description: 'brak opisu',
}

Card.propTypes = {
  img: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  releaseDate: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
};

export default Card;
