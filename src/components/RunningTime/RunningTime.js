import React from 'react'
import styles from './RunningTime.module.scss'
import PropTypes from 'prop-types'
import clockIcon from '../../images/time_icon.svg'


const RunningTime = ({duration}) => {
  return (
    <span className={styles.container}>
      <span>Czas trwania:</span>
      <img src={clockIcon} alt="mała ikona zegara"/>
      <span className={styles.duration}>{parseInt(duration / 60)} min</span>
    </span>
  )
}

RunningTime.propTypes = {
  duration: PropTypes.number.isRequired,
}

export default RunningTime
