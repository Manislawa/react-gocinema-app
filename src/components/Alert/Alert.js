import React from 'react'
import styles from './Alert.module.scss'

const iconType = {
  success: 'check-circle',
  error: 'exclamation-circle',
  warning: 'exclamation-triangle',
  info: 'info-circle'
}

const Alert = props => {
  const { message, type } = props.alert;
  console.log(type)
  return (
    <div className={`${styles.container} ${styles[type]}`}>
      <img src={require(`../../images/alert_icons/${iconType[type]}.svg`)} className={styles.icon} alt="ikonka powiadomienia"/>
      <div className={styles.message}>{message}!!</div>
    </div>
  )
}



export default Alert

