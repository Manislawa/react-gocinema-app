import React from "react";
import PropTypes from "prop-types";
import styles from "./Button.module.scss";

const Button = ({ soldedOut }) => {
  return (
    <div>
      {soldedOut ? (
        <span className={styles.inaccessible}>niedostępny</span>
      ) : (
        <button className={styles.button}>Rezerwuj</button>
      )}
    </div>
  );
};

Button.propTypes = {
  soldedOut: PropTypes.bool.isRequired
};

export default Button;
